# Start minikube
echo "STARTING MINIKUBE..."
minikube start
echo ""

# Set docker env
echo "SETTING DOCKER ENV..."
eval $(minikube docker-env)
echo ""

# Build image
echo "NOW BUILDING THE HELLOWORLD IMAGE..."
docker build -t helloworld .
echo ""

# Deploy the image into Kube cluster
echo "DEPLOYING THE IMAGE INTO THE KUBE CLUSTER..."
kubectl apply -f helloworld.yaml
echo ""

# Wait for the deployment to be completed
echo "WAITING FOR THE DEPLOYMENT TO BE COMPLETED..."
kubectl rollout status deployment/helloworld
echo ""

# Run curl 10 times in order to see the request is served by a different container
echo "SENDING 10 REQUESTS TO HELLOWORLD SERVICE IN ORDER TO SEE EACH ONE IS SERVICED BY DIFFERENT PODS..."
for i in 1 2 3 4 5 6 7 8 9 10; do
  echo "Iteration no $i:"
  curl $(minikube service helloworld --url)
  sleep 1
done
