FROM python:3

RUN pip install flask

COPY ./server.py .

CMD [ "/usr/local/bin/python", "./server.py" ]
