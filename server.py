from flask import Flask
import os
import socket

hname= socket.gethostname()
ostr= "My hostname is: " + hname + ". Hello World!\n"

app = Flask(__name__)

@app.route('/')
def hello():
    return ostr

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=False)




