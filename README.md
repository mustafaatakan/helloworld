- Prerequisities: minikube, curl and docker should be installed

- Just download the code by running
"git clone https://mustafaatakan@bitbucket.org/mustafaatakan/helloworld.git/"

- Go to the helloworld directory and run "./run.sh"